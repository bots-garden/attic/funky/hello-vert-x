# build
FROM maven:3.6.0-jdk-8-alpine AS build
COPY . .
# --- build, then run the application
CMD /bin/bash -c "mvn clean package; cp target/*-fat.jar app.jar; java -jar ./app.jar"
