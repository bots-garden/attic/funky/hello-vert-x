package garden.bots.ping

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj

class MainVerticle : AbstractVerticle() {

  override fun stop(stopFuture: Future<Void>) {
    super.stop()
  }

  override fun start(startPromise: Promise<Void>) {

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    router.get("/api/hello").handler { context ->
      context.response().putHeader("content-type", "application/json;charset=UTF-8")
        .end(
          json { obj("message" to "🖖 Hello World 🌍😉") }.encodePrettily()
        )
    }

    // serve static assets
    router.route("/*").handler(StaticHandler.create().setCachingEnabled(false))

    // internal port
    val httpPort = System.getenv("PORT")?.toInt() ?: 8080

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listen(httpPort) { http ->
        when {
          http.failed() -> {
            startPromise.fail(http.cause())
          }
          http.succeeded() -> {
            println("🏓 Ping started on $httpPort")
            println("🌍 HTTP server started on port ${httpPort}")
            startPromise.complete()
          }
        }
      }
  }
}
